FROM mcr.microsoft.com/dotnet/sdk:7.0 

RUN apt-get update
RUN curl -fsSL https://deb.nodesource.com/setup_21.x --ssl-no-revoke | bash - &&\
apt-get install -y nodejs

COPY ./DotnetTemplate.Web /opt/dotnet/DotnetTemplate.Web/
WORKDIR /opt/dotnet/DotnetTemplate.Web
RUN dotnet build

ENTRYPOINT ["dotnet", "run"]